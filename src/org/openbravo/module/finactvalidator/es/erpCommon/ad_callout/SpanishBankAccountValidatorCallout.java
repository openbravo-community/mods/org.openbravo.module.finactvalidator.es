/*
 * ************************************************************************ 
 * The contents of this file are subject to the Openbravo Public License Version 1.1
 * (the "License"), being the Mozilla Public License Version 1.1 with a
 * permitted attribution clause; you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 * http://www.openbravo.com/legal/license.html Software distributed under the
 * License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the License for the specific language
 * governing rights and limitations under the License. The Original Code is
 * Openbravo ERP. The Initial Developer of the Original Code is Openbravo SL All
 * portions are Copyright (C) 2020 Openbravo SL All Rights Reserved.
 * Contributor(s): ______________________________________.
 * ***********************************************************************
 */

package org.openbravo.module.finactvalidator.es.erpCommon.ad_callout;

import javax.servlet.ServletException;

import org.openbravo.erpCommon.ad_callouts.SimpleCallout;
import org.openbravo.erpCommon.utility.OBMessageUtils;

abstract class SpanishBankAccountValidatorCallout extends SimpleCallout {
  abstract String getAccountToValidate(CalloutInfo info);

  @Override
  protected void execute(CalloutInfo info) throws ServletException {
    if (!SpanishBankAccountValidator.isValid(getAccountToValidate(info))) {
      final String message = OBMessageUtils.messageBD("FNACTV_BankAcountNoValid");
      info.addResult("WARNING", message);
    }
  }
}
