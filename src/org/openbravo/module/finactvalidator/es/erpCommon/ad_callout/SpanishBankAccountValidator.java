/*
 * ************************************************************************ 
 * The contents of this file are subject to the Openbravo Public License Version 1.1
 * (the "License"), being the Mozilla Public License Version 1.1 with a
 * permitted attribution clause; you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 * http://www.openbravo.com/legal/license.html Software distributed under the
 * License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the License for the specific language
 * governing rights and limitations under the License. The Original Code is
 * Openbravo ERP. The Initial Developer of the Original Code is Openbravo SL All
 * portions are Copyright (C) 2020 Openbravo SL All Rights Reserved.
 * Contributor(s): ______________________________________.
 * ***********************************************************************
 */

package org.openbravo.module.finactvalidator.es.erpCommon.ad_callout;

class SpanishBankAccountValidator {

  /*
   * @param strCCC, string with bank account number to validate
   * 
   * @return boolean true if code is correct
   */
  static boolean isValid(String strCCC) {
    // length check
    if (strCCC.length() != 20) {
      return false;
    }
    try {
      // 8 first digits design bank and branch codes
      long lEntidadSucursal = Long.valueOf(strCCC.substring(0, 8));
      // bank with code 0 is removed, as it doesn't exist
      if (lEntidadSucursal == 0) {
        return false;
      }
      // digit control numbers retrieved
      int iDigitoControl = Integer.valueOf(strCCC.substring(8, 10));
      // 10 last numbers corresponds to the bank account
      long lCuenta = Long.valueOf(strCCC.substring(10, 20));
      // digit controls are validated
      if (digitosControl(lEntidadSucursal, lCuenta) != iDigitoControl) {
        return false;
      }
    } catch (NumberFormatException ex) {
      return false;
    }
    return true;
  }

  /**
   * Calculates the two digit control numbers of a bank account
   * 
   * @param lEntidadSucursal
   *          bank and branch codes
   * @param lCuenta
   *          bank account number
   * @return int, two digits number with digit control numbers
   */
  private static int digitosControl(long lEntidadSucursal, long lCuenta) {
    // first digit control is calculated
    long d = dc(lEntidadSucursal);
    d = d * 10;
    // second digit control calculated and added to first one
    d += dc(lCuenta);
    return (int) d;
  }

  /**
   * For a given number, calculates its digit control
   * 
   * @param i
   *          number from which calculate digit control
   * @return int calculated digit control
   */
  private static int dc(long i) {
    long localI = i;
    // weight vector
    int[] pesos = { 6, 3, 7, 9, 10, 5, 8, 4, 2, 1 };
    int contador = 0;
    long s = 0, d;
    while (localI != 0) {
      d = localI % 10;
      localI = localI / 10;
      s += d * pesos[contador];
      contador++;
    }
    int resultado = (int) (11 - (s % 11));
    if (resultado == 10) {
      resultado = 1;
    } else if (resultado == 11) {
      resultado = 0;
    }
    return resultado;
  }
}
